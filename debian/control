Source: libkexiv2
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Pino Toscano <pino@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper (>= 9),
               kde-sc-dev-latest (>= 4:4.12),
               kdelibs5-dev,
               libexiv2-dev (>= 0.20),
               pkg-config,
               pkg-kde-tools (>= 0.12)
Standards-Version: 3.9.6
Homepage: http://www.kde.org/
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-kde/applications/libkexiv2.git
Vcs-Git: git://anonscm.debian.org/pkg-kde/applications/libkexiv2.git

Package: libkexiv2-11
Section: libs
Architecture: any
Depends: libkexiv2-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Qt like interface for the libexiv2 library
 Libkexiv2 is a Qt wrapper around the Exiv2 library, used to manipulate
 pictures metadata.
 .
 This package contains the libkexiv2 library.

Package: libkexiv2-data
Architecture: all
Depends: ${misc:Depends}
Breaks: kdegraphics-libs-data (<< 4:4.6.80)
Replaces: kdegraphics-libs-data (<< 4:4.6.80)
Description: Qt-like interface for the libexiv2 library -- data files
 Libkexiv2 is a Qt wrapper around the Exiv2 library, used to manipulate
 pictures metadata.
 .
 This package contains the data files of the libexiv2 library.

Package: libkexiv2-dev
Section: libdevel
Architecture: any
Depends: libkexiv2-11 (= ${binary:Version}),
         pkg-config,
         ${misc:Depends},
         ${sameVersionDep:kdelibs5-dev:libkexiv2-11}
Conflicts: libkexiv2-6-dev, libkexiv2-7-dev, libkexiv2-8-dev
Description: Qt-like interface for the libexiv2 library -- development files
 Libkexiv2 is a Qt wrapper around the Exiv2 library, used to manipulate
 pictures metadata.
 .
 This package contains the development files and the documentation.
 The library documentation is available in the kexiv2.h header file.

Package: libkexiv2-dbg
Section: debug
Architecture: any
Priority: extra
Depends: libkexiv2-11 (= ${binary:Version}), ${misc:Depends}
Description: Qt-like interface for the libexiv2 library -- debugging symbols
 Libkexiv2 is a Qt wrapper around the Exiv2 library, used to manipulate
 pictures metadata.
 .
 This package contains the debugging files used to investigate problems with
 the libexiv2 library.
